package model.data_structures;

public class LinkedList<T> {
	private Node<T> first;
	private int size;

	public LinkedList()
	{
		first = null;
	}

	public LinkedList(T item)
	{
		first = new Node<T>(item);
		size = 1;
	}

	public void addFirst(T item)
	{
		Node<T> newFirst = new Node<T>(item);
		newFirst.setNext(first);
		first = newFirst;
		size++;
	}

	public void addLast(T item)
	{
		Node<T> newLast = new Node<T>(item);
		if(first == null)
		{
			first = newLast;
		}
		else
		{
			Node<T> actual = first;
			while(actual.getNext()!= null)
			{
				actual = actual.getNext();
			}
			actual.setNext(newLast);
		}
		size++;
	}

	public boolean add(T item, int pos) throws Exception
	{
		Node<T> newNode = new Node<T>(item);
		int contador = 0;

		if(first == null)
		{
			throw new Exception("La lista est� vac�a");
		}

		else
		{
			if(pos == 1){
				addFirst(item);
				return true;
			}
			else if(pos == size){
				addLast(item);
				return true;
			}
			else{
				Node<T> actual = first;
				while(actual.getNext()!= null)
				{
					contador++;
					if(contador == pos)
					{
						newNode.setNext(actual.getNext());
						actual.setNext(newNode);
						size++;
						return true;
					}
				}	
				throw new Exception("No se encontr� la posici�n dada");
			}
		}		
	}

	public void removeFirst() throws Exception
	{
		if(first == null)
		{
			throw new Exception("La lista est� vac�a");
		}
		else
		{
			Node<T> actual = first;
			first = first.getNext();
			actual.setNext(null);
			size--;
		}
	}

	public void removeLast() throws Exception
	{
		if(first == null)
		{
			throw new Exception("La lista est� vac�a");
		}
		else
		{
			Node<T> actual = first;
			while(actual.getNext().getNext() != null)
			{
				actual = actual.getNext();
			}
			actual.setNext(null);
			size--;
		}
	}

	public boolean remove(int pos) throws Exception
	{
		int contador = 0;
		if(first == null)
		{
			throw new Exception("La lista est� vac�a");
		}
		else
		{
			if(pos == 1){
				removeFirst();
				return true;
			}
			else if(pos == size){
				removeLast();
				return true;
			}
			else{
				Node<T> actual = first;
				contador++;
				while(actual.getNext() != null)
				{
					if(contador+1 == pos)
					{
						Node<T> nextNull = actual.getNext();
						actual.setNext(actual.getNext().getNext());
						nextNull.setNext(null);
						size--;
						return true;
					}
					actual = actual.getNext();
				}
				throw new Exception("No se encontr� la posici�n dada");
			}
		}
	}

	public int getSize()
	{
		return size;
	}

}
