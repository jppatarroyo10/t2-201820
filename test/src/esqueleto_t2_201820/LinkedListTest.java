package esqueleto_t2_201820;

import static org.junit.Assert.*;
import model.data_structures.LinkedList;

import org.junit.Test;

public class LinkedListTest {
	
	LinkedList<Integer> lista;
	  
	
	@Test
	public void getSizeTest1() throws Exception{
		lista = new LinkedList<Integer>();
		lista.addFirst(1);
		lista.addLast(3);
		lista.add(2, 2);
		assertEquals(3,lista.getSize(),0);
	}
	
	@Test
	public void getSizeTest2() throws Exception{
		lista = new LinkedList<Integer>();
		lista.addFirst(1);
		lista.addLast(3);
		lista.add(2, 2);
		lista.add(4,3);
		lista.removeFirst();
		lista.removeLast();
		lista.remove(2);
		assertEquals(1,lista.getSize(),0);
	}

}
